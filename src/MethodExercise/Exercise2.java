package MethodExercise;

import java.util.Scanner;

public class Exercise2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the first number");
        double x = input.nextDouble();
        System.out.println("Enter the second number");
        double y = input.nextDouble();
        System.out.println("Enter the third number");
        double z = input.nextDouble();

        System.out.println("The number average: "+Numberaverage(x,y,z));



    }

    public static double Numberaverage(double x, double y, double z) {
        return((x+y+z)/3);

    }
}
